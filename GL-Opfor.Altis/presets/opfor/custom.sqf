/*
Needed Mods:
- None

Optional Mods:
- None
*/

// Enemy infantry classes
opfor_officer = "B_officer_F";									//Officer
opfor_squad_leader = "B_Patrol_Soldier_TL_F";							//Squad Leader
opfor_team_leader = "B_Patrol_Soldier_TL_F";							//Team Leader
opfor_sentry = "B_Patrol_Soldier_UAV_F";								//Rifleman (Lite)
opfor_rifleman = "B_Patrol_Soldier_AR_F";								//Rifleman
opfor_rpg = "B_recon_LAT_F";											//Rifleman (LAT)
opfor_grenadier = "B_Soldier_GL_F";										//Grenadier
opfor_machinegunner = "B_Patrol_Soldier_MG_F";							//Autorifleman
opfor_heavygunner = "B_Patrol_HeavyGunner_F";							//Heavy Gunner
opfor_marksman = "B_Patrol_Soldier_M_F";								//Marksman
opfor_sharpshooter = "B_Patrol_Soldier_M_F";							//Sharpshooter
opfor_sniper = "B_ghillie_sard_F";										//Sniper
opfor_at = "B_Patrol_Soldier_AT_F";										//AT Specialist
opfor_aa = "B_Soldier_AA_F";											//AA Specialist
opfor_medic = "B_Patrol_Medic_F";										//Combat Life Saver
opfor_engineer = "B_Patrol_Engineer_F";									//Engineer
opfor_paratrooper = "B_Patrol_Soldier_AR_F";							//Paratrooper
opfor_VLead = "B_Patrol_Soldier_TL_F";									//Viper Leader
opfor_V = "B_Patrol_Soldier_AR_F";										//Viper
opfor_VMed = "B_Patrol_Medic_F";										//Viper Medic
opfor_VAT = "B_Patrol_Soldier_AT_F";									//Viper AT



// Enemy vehicles used by secondary objectives.
opfor_mrap = "B_MRAP_01_F";												//Ifrit
opfor_mrap_armed = "B_MRAP_01_hmg_F";									//Ifrit (HMG)
opfor_transport_helo = "B_Heli_Transport_01_F";							//Mi-290 Taru (Bench)
opfor_transport_truck = "B_Truck_01_covered_F";							//Tempest Transport (Covered)
opfor_ammobox_transport = "B_Truck_01_transport_F";						//Tempest Transport (Open) -> Has to be able to transport resource crates!
opfor_fuel_truck = "B_Truck_01_fuel_F";									//Tempest Fuel
opfor_ammo_truck = "B_Truck_01_ammo_F";									//Tempest Ammo
opfor_fuel_container = "B_Slingload_01_fuel_F";				//Taru Fuel Pod
opfor_ammo_container = "B_Slingload_01_ammo_F";				//Taru Ammo Pod
opfor_flag = "Flag_US_F";												//CSAT Flag

/* Adding a value to these arrays below will add them to a one out of however many in the array, random pick chance.
Therefore, adding the same value twice or three times means they are more likely to be chosen more often. */

/* Militia infantry. Lightweight soldier classnames the game will pick from randomly as sector defenders.
Think of them like garrison or military police forces, which are more meant to control the local population instead of fighting enemy armies. */
militia_squad = [
	"B_Patrol_Soldier_AR_F",											//Rifleman (Lite)
	"B_Patrol_Soldier_AR_F",											//Rifleman (Lite)
	"B_Patrol_Soldier_AR_F",											//Rifleman VIPER
	"B_Patrol_Soldier_AR_F",											//Rifleman VIPER
	"B_Patrol_Soldier_AT_F",											//Rifleman VIPER
	"B_Patrol_Soldier_AT_F",											//Rifleman (AT)
	"B_Patrol_HeavyGunner_F",											//Autorifleman
	"B_Patrol_Soldier_M_F",												//Marksman
	"B_Patrol_Medic_F",													//Medic
	"B_Patrol_Engineer_F"												//Engineer
];

// Militia vehicles. Lightweight vehicle classnames the game will pick from randomly as sector defenders.
militia_vehicles = [
	"B_LSV_01_armed_F"													//Qilin (armed)
];

// All enemy vehicles that can spawn as sector defenders and patrols at high enemy combat readiness (aggression levels).
opfor_vehicles = [
	"B_MRAP_01_hmg_F",													//Ifrit (HMG)
	"B_MRAP_01_hmg_F",													//Ifrit (GMG)
	"B_MRAP_01_gmg_F",													//Ifrit (GMG)
	"B_LSV_01_AT_F",													//Qilin (AT)
	"B_APC_Tracked_01_rcws_F",										//BTR-K Kamysh
	"B_APC_Tracked_01_rcws_F",										//BTR-K Kamysh
	"B_APC_Tracked_01_AA_F",											//ZSU-39 Tigris
	"B_APC_Tracked_01_AA_F",											//ZSU-39 Tigris
	"B_MBT_01_cannon_F",												//T-100 Varsuk
	"B_AFV_Wheeled_01_up_cannon_F",										//T-100 Varsuk
	"B_MBT_01_TUSK_F",												//T-140 Angara
	"B_MBT_01_TUSK_F",												//T-140K Angara
	"B_MBT_01_TUSK_F"													//Artillery
];

// All enemy vehicles that can spawn as sector defenders and patrols but at a lower enemy combat readiness (aggression levels).
opfor_vehicles_low_intensity = [
	"B_MRAP_01_hmg_F",													//Ifrit (HMG)
	"O_MRAP_01_hmg_F",													//Ifrit (HMG)
	"B_LSV_01_AT_F",													//Qilin (AT)
	"B_APC_Wheeled_01_cannon_F",										//MSE-3 Marid 
	"B_AFV_Wheeled_01_up_cannon_F",										//BTR-K Kamysh
	"B_APC_Tracked_01_rcws_F"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at high enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles = [
	"B_MRAP_01_hmg_F",													//Ifrit (HMG)
	"B_MRAP_01_gmg_F",													//Ifrit (GMG)
	"B_MRAP_01_gmg_F",													//Ifrit (GMG)
	"B_LSV_01_AT_F",													//Qilin (AT)
	"B_Truck_01_transport_F",											//Tempest Transport
	"B_Truck_01_covered_F",												//Tempest Transport (Covered)
	"B_APC_Tracked_01_rcws_F",										//BTR-K Kamysh
	"B_APC_Tracked_01_rcws_F",										//BTR-K Kamysh
	"B_AFV_Wheeled_01_up_cannon_F",
	"B_AFV_Wheeled_01_up_cannon_F",
	"B_APC_Tracked_01_AA_F",											//ZSU-39 Tigris
	"B_APC_Tracked_01_AA_F",											//ZSU-39 Tigris
	"B_MBT_01_cannon_F",												//T-100 Varsuk
	"B_MBT_01_TUSK_F",												//T-100 Varsuk
	"B_MBT_01_TUSK_F",												//T-140 Angara
	"B_MBT_01_TUSK_F",												//T-140K Angara
	"B_Heli_Attack_01_F",											//Po-30 Orca (Armed)
	"B_Heli_Attack_01_F",									//Po-30 Orca (Armed)
	"B_Heli_Attack_01_F",										//Mi-290 Taru (Bench)
	"B_Heli_Attack_01_F",								//Mi-48 Kajman
	"B_Heli_Transport_01_F",											//Xi'an (Infantry)
	"B_AFV_Wheeled_01_up_cannon_F",											//Artillery
	"B_APC_Wheeled_01_cannon_F"
];

// All enemy vehicles that can spawn as battlegroups, either assaulting or as reinforcements, at lower enemy combat readiness (aggression levels).
opfor_battlegroup_vehicles_low_intensity = [
	"B_MRAP_01_hmg_F",													//Ifrit (HMG)
	"B_MRAP_01_hmg_F",													//Ifrit (HMG)
	"B_Truck_01_transport_F",											//Tempest Transport
	"B_APC_Wheeled_01_cannon_F",											//MSE-3 Marid 
	"B_APC_Tracked_01_rcws_F",										//BTR-K Kamysh
	"B_APC_Tracked_01_AA_F",											//ZSU-39 Tigris
	"B_Heli_Attack_01_F",									//Po-30 Orca (Armed)
	"B_Heli_Transport_01_F"										//Mi-290 Taru (Bench)
];

/* All vehicles that spawn within battlegroups (see the above 2 arrays) and also hold 8 soldiers as passengers.
If something in this array can't hold all 8 soldiers then buggy behaviours may occur.	*/
opfor_troup_transports = [
	"B_APC_Tracked_01_rcws_F",											//Tempest Transport
	"B_APC_Wheeled_01_cannon_F",												//Tempest Transport (Covered)
	"B_APC_Tracked_01_rcws_F",											//MSE-3 Marid 
	"B_Heli_Transport_01_F"										//Mi-290 Taru (Bench)
];

// Enemy rotary-wings that will need to spawn in flight.
opfor_choppers = [
	"B_Heli_Transport_01_F",										//Mi-290 Taru (Bench)
	"B_Heli_Attack_01_F"									//Po-30 Orca (Armed)
];

// Enemy fixed-wings that will need to spawn in the air.
opfor_air = [
	"B_Plane_CAS_01_F",									//To-199 Neophron (CAS)
	"B_Plane_Fighter_01_F",												//To-201 Shikra
	"B_Plane_Fighter_01_Stealth_F"
];
