private [ "_commanderobj" ];

_commanderobj = objNull;

if (!isNil "PlatoonSergeant") then {
	{ if ( _x == PlatoonSergeant ) exitWith { _commanderobj = _x }; } foreach allPlayers;
};

_commanderobj