handleHolsterKey = (findDisplay 46) displayAddEventHandler ["KeyDown",{
    params ["_controlID","_keyCode","_shiftState","_CtrlAltState"];

    if (_keyCode == 7 && (primaryWeapon player != "" || secondaryWeapon player != "")) then {
        player action ["SwitchWeapon",player,player,-1];
    };
}];